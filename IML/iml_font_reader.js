var fs = require("fs");
var Buffer = require("buffer").Buffer;
var filename;
var stats;
var fileSizeInBytes;

if (typeof(process.argv[2]) === "undefined") {
    console.log("Use: node iml_font_reader.js fontFilename.fon");
    process.exit(1);
} else {
    filename = process.argv[2];
    stats = fs.statSync(filename);
    fileSizeInBytes = stats["size"];
}

function getHextetFrom(decimal) {
    var binary = decimal.toString(2);

    while (binary.length < 16) {
        binary = "0" + binary;
    }

    return binary.split("").reverse().map(function (x) {
        return x === "1";
    });
}

function Glyph(ascii, width, nbCol, height, bitmapBuffer, sizeCol, indirectionOffset) {

    var bitmap = [];
    var tmpBitmap = [];
    var startOffset = indirectionOffset * nbCol * sizeCol * 2;
    var endOffset = startOffset + nbCol * sizeCol * 2;
    var glyphBitmapBuffer = bitmapBuffer.slice(startOffset, endOffset);

    for (var i = 0; i < glyphBitmapBuffer.length / 2; i++) {

        var hextet = getHextetFrom(glyphBitmapBuffer.readUInt16LE(i * 2));

        // Performance reason! Faster that run Array.concat per loop
        hextet.forEach(function (bit) {
            tmpBitmap.push(bit);
        });
    }

    // Prepare bitmap
    for (var y = height - 1; y >= 0; y--) {
        var line = [];

        for (var x = 0; x < width; x++) {
            line.push(tmpBitmap[x * sizeCol * 8 * 2 + y]);
        }
        bitmap.push(line);
    }

    return {
        ascii: ascii,
        width: width,
        height: height,
        bitmap: bitmap,
        display: function () {

            console.log("ascii:", ascii, "width:", width, "height:", height);

            for (var y = 0; y < this.height; y++) {
                console.log(
                    this.bitmap[y]
                        .map(function (element) {
                            return element === true ? "#" : " ";
                        })
                        .join("")
                );
            }
        }
    }
}

function processPageInfo(buffer, index, fonts, firstPageExtracted) {
    var utfCharCodeOffset = (buffer.readUInt16LE(index) << 8) + buffer.readUInt16LE(index + 2);

    if (utfCharCodeOffset === 0 && firstPageExtracted) {
        return fonts;
    }

    var offsetNextPageSection = buffer.readUInt32LE(index + 4);
    var nbCol = buffer.readUInt16LE(index + 8);
    var nbLine = buffer.readUInt16LE(index + 10);
    var sizeChar = buffer.readUInt16LE(index + 12);
    var sizeCol = buffer.readUInt16LE(index + 14);
    var indirectionTable = buffer.slice(index + 16, index + 272);
    var widthTable = buffer.slice(index + 272, index + 528);

    console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
    console.log("utfCharCodeOffset: ", utfCharCodeOffset);
    console.log("offsetNextPageSection:", offsetNextPageSection);
    console.log("nbCol:", nbCol);
    console.log("nbLine:", nbLine);
    console.log("sizeChar:", sizeChar);
    console.log("sizeCol:", sizeCol);

    var bitmapBuffer = buffer.slice(index + 528, index + 528 + (offsetNextPageSection * 2));

    for (var i = 0; i < indirectionTable.length; i++) {
        if (indirectionTable[i]) {

            console.log("Chr: ", String.fromCharCode(utfCharCodeOffset + i), ", graphical index: ", i, ", width: ",
                widthTable[i], "indirectionTable: ", indirectionTable[i]);

            fonts[i] = new Glyph(utfCharCodeOffset + i, widthTable[i], nbCol, nbLine, bitmapBuffer, sizeCol,
                indirectionTable[i]);
        }
    }

    return processPageInfo(buffer, index + 528 + (offsetNextPageSection * 2), fonts, true);
}

fs.open(filename, "r", function (status, fd) {
    if (status) {
        console.log(status.message);
        return;
    }

    var buffer = new Buffer(fileSizeInBytes, "binary");

    fs.read(fd, buffer, 0, buffer.length, 0, function (err, bytesRead) {

        var version = buffer.toString("utf8", 0, 12);
        var signature = buffer.toString("utf8", 12, 16);

        console.log("version:", version);
        console.log("signature:", signature);
        var fonts = processPageInfo(buffer, 16, {}, false);

        for (var char in fonts) {
            console.log("Char: ", String.fromCharCode(fonts[char].ascii));
            fonts[char].display();
        }
    });
});