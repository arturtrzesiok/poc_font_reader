var fs = require("fs");
var Buffer = require("buffer").Buffer;
var filename;
var stats;
var fileSizeInBytes;

if (typeof(process.argv[2]) === "undefined") {
    console.log("Use: node ims_font_reader.js fontFilename.pol");
    process.exit(1);
} else {
    filename = process.argv[2];
    stats = fs.statSync(filename);
    fileSizeInBytes = stats["size"];
}

function getHextetFrom(decimal) {
    var binary = decimal.toString(2);

    while (binary.length < 16) {
        binary = "0" + binary;
    }

    return binary.split("").reverse().map(function (x) {
        return x === "1";
    });
}

function Glyph(ascii, width, nbCol, height, bitmapBuffer, sizeCol, indirectionOffset) {

    var bitmap = [];
    var tmpBitmap = [];
    var startOffset = indirectionOffset * nbCol * sizeCol * 2;
    var endOffset = startOffset + nbCol * sizeCol * 2;
    var glyphBitmapBuffer = bitmapBuffer.slice(startOffset, endOffset);

    for (var i = 0; i < glyphBitmapBuffer.length / 2; i++) {
        var hextet = getHextetFrom(glyphBitmapBuffer.readUInt16BE(i * 2));

        // Performance reason! Faster that run Array.concat per loop
        hextet.forEach(function (bit) {
            tmpBitmap.push(bit);
        });
    }

    // Prepare bitmap
    for (var y = height - 1; y >= 0; y--) {
        var line = [];

        for (var x = 0; x < width; x++) {
            line.push(tmpBitmap[x * sizeCol * 8 * 2 + y]);
        }
        bitmap.push(line);
    }

    return {
        ascii: ascii,
        width: width,
        height: height,
        bitmap: bitmap,
        display: function () {

            console.log("ascii:", ascii, "width:", width, "height:", height);

            for (var y = 0; y < this.height; y++) {
                console.log(
                    this.bitmap[y]
                        .map(function (element) {
                            return element === true ? "#" : " ";
                        })
                        .join("")
                );
            }
        }
    }
}

fs.open(filename, "r", function (status, fd) {
    if (status) {
        console.log(status.message);
        return;
    }

    var buffer = new Buffer(fileSizeInBytes, "binary");
    var fonts = {};

    fs.read(fd, buffer, 0, buffer.length, 0, function (err, bytesRead) {

        // All offsets are static in this file format
        var version = buffer.toString("utf8", 0, 10);
        var nbCol = buffer.readInt16BE(10);
        var nbLine = buffer.readInt16BE(12);
        var sizeChar = buffer.readInt16BE(14);
        var sizeCol = buffer.readInt16BE(16);

        var indirectionTable = buffer.slice(18, 274);
        var widthTable = buffer.slice(274, 530);

        console.log("version:", version);
        console.log("nbCol:", nbCol);
        console.log("nbLine:", nbLine);
        console.log("sizeChar:", sizeChar);
        console.log("sizeCol:", sizeCol);

        var bitmapBuffer = buffer.slice(530, buffer.length);

        for (var i = 0; i < indirectionTable.length; i++) {
            if (indirectionTable[i]) {
                console.log("Chr: ", String.fromCharCode(i), ", graphical index: ", i, ", width: ", widthTable[i],
                    "indirectionTable: ", indirectionTable[i]);

                fonts[i] = new Glyph(i, widthTable[i], nbCol, nbLine, bitmapBuffer, sizeCol, indirectionTable[i]);
            }
        }

        for (var char in fonts) {
            console.log("Char: ", String.fromCharCode(fonts[char].ascii));
            fonts[char].display();
        }
    });
});